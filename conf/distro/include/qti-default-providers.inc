#
# Default virtual providers
#
PREFERRED_PROVIDER_virtual/kernel  ?= "linux-msm"
PREFERRED_PROVIDER_virtual/egl      = "adreno"
PREFERRED_PROVIDER_virtual/libgles1 = "adreno"
PREFERRED_PROVIDER_virtual/libgles2 = "adreno"
PREFERRED_PROVIDER_virtual/mkbootimg-native ?= "mkbootimg-native"
PREFERRED_PROVIDER_virtual/mkdtimg-native ?= "mkdtimg-native"
PREFERRED_PROVIDER_virtual/kernel-toolchain-native ?= "kernel-toolchain-native"
PREFERRED_PROVIDER_virtual/dtc-native ?= "dtc-native"

#
# Default virtual runtime providers
#

#
# Default recipe providers
#
